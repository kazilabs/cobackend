# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import resumeparser.utils.validator
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0003_auto_20170428_1712'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='resume',
            name='file_id',
        ),
        migrations.AddField(
            model_name='resume',
            name='datafile',
            field=models.FileField(default=' ', upload_to=b'uploads/resumes/', validators=[resumeparser.utils.validator.validate_file_extension]),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='resume',
            name='uploaded',
            field=models.DateTimeField(default=datetime.datetime(2017, 4, 28, 17, 47, 15, 501530, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='ResumeArchive',
        ),
    ]
