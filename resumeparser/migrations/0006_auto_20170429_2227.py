# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0005_resume_rank'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resume',
            name='datafile',
            field=models.FileField(upload_to=b'uploads/resumes/'),
        ),
    ]
