# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resumeparser', '0011_resume_industry'),
    ]

    operations = [
        migrations.AddField(
            model_name='resume',
            name='processing',
            field=models.CharField(default='Completed', max_length=70),
            preserve_default=False,
        ),
    ]
