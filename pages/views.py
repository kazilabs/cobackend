from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.template import TemplateDoesNotExist, RequestContext, loader
from django.core.mail import EmailMessage
from django.utils.translation import ugettext as _
from django.db.models import Count
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q

from datetime import datetime

from pages.models import *
from pages.forms import *
from pages.views import *

def page(request, parent=None, path=""):
    path = path.replace("/", "")
    context = RequestContext(request)
    if parent:
        context['page'] = page = get_object_or_404(Page, path=path, parent__path=parent)
        context['parent'] = context['page'].parent
    else:
        context['page'] = page = get_object_or_404(Page, path=path, parent=None)

    template_name = 'page.html'

    if page.homepage:
        template_name = 'home.html'

    if page.path == "privacy":
        template_name = 'privacy.html'

    try:
        loader.get_template(template_name)
    except TemplateDoesNotExist:
        template_name = 'page.html'

    return render_to_response(template_name, context)
