from django.db import models
from django.conf import settings
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

from django.template import RequestContext, Context, loader


import os
import datetime
import socket

class Page(models.Model):
    name = models.CharField(max_length=64, help_text='Used for navigation menus',)
    title = models.CharField(max_length=64, help_text='Used for the browser title bar',)
    header = models.CharField(max_length=128, help_text='Appears at the top of the page',)
    description = models.TextField(blank=True, help_text='Appears below the title in Google search results. Important for search engines. Should be two or three sentences.',)
    content = models.TextField(blank=True,)

    sortorder = models.IntegerField(db_index=True, blank=True, null=True,)
    parent = models.ForeignKey('self', blank=True, null=True,)
    path = models.SlugField(max_length=64, default='', help_text='Do not change the path of existing pages', db_index=True,)

    homepage = models.BooleanField(default=False, verbose_name="Is this the homepage?",)
    top_menu = models.BooleanField(default=True,)
    footer_menu = models.BooleanField(default=False,)
    visible = models.BooleanField(default=True,)

    def save(self, *args, **kwargs):
        if self.homepage:
            self.path = ""
            self.feature = False
            try:
                temp = Page.objects.get(homepage=True)
                if self != temp:
                    temp.homepage = False
                    if temp.path == "":
                        temp.path = slugify(temp.display)
                    temp.save()
            except Page.DoesNotExist:
                pass
        if not self.homepage:
            self.path = slugify(self.name)

        super(Page, self).save(*args, **kwargs)

    class Meta:
        ordering = ['sortorder', 'name',]
        unique_together = (('path', 'parent',),)

    def __unicode__(self):
        if self.parent:
            full_display = ['', self.name]

            def get_parent_display(current_page):
                if current_page.parent:
                    full_display.insert(1, current_page.parent.name + ' :: ')
                    get_parent_display(current_page.parent)
            get_parent_display(self)
            return ''.join(full_display)

        return self.name

    def get_absolute_url(self):
        if self.parent:
            full_path = ['']
            if self.path:
                full_path.append('/' + self.path)
            def get_parent_path(current_page):
                if current_page.parent:
                    if current_page.parent.path:
                        full_path.insert(1, '/' + current_page.parent.path)
                    get_parent_path(current_page.parent)
            get_parent_path(self)
            return ''.join(full_path)

        return '/' + self.path

    def get_child_pages(self):
        return Page.objects.filter(parent=self, visible=True)


    def get_previous_page(self):
        ids = list(Page.objects.filter(parent=self.parent, visible=True,).values_list("id", flat=True))
        current_idx = ids.index(self.id)
        if current_idx == 0:
            return None
        return Page.objects.get(id=ids[current_idx - 1])

    def get_next_page(self):
        ids = list(Page.objects.filter(parent=self.parent, visible=True,).values_list("id", flat=True))
        current_idx = ids.index(self.id)
        if current_idx + 1 == len(ids):
            return None
        return Page.objects.get(id=ids[current_idx + 1])
