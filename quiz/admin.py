from quiz.models import Quiz, Question, Response, QuizAnswers, QuizResponse, Personality
from django.contrib import admin

import nested_admin


class ResponseInline(nested_admin.NestedTabularInline):
    model = Response
    extra = 0
    max_num = 2
    min_num = 2


class QuestionInline(nested_admin.NestedTabularInline):
    model = Question
    extra = 0
    inlines = [ResponseInline, ]


class QuizAdmin(nested_admin.NestedModelAdmin):
    model = Quiz
    inlines = [QuestionInline, ]


class QuizAnswersInline(nested_admin.NestedTabularInline):
    model = QuizAnswers
    extra = 0


class QuizResponseAdmin(nested_admin.NestedModelAdmin):
    model = QuizResponse
    inlines = [QuizAnswersInline, ]


class PersonalityAdmin(admin.ModelAdmin):
    model = Personality
    list_display = ['name', 'description', 'sortorder']
    ordering = ['sortorder', 'name']

admin.site.register(Quiz, QuizAdmin)
admin.site.register(QuizResponse, QuizResponseAdmin)
admin.site.register(Personality, PersonalityAdmin)
