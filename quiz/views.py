from django.http import Http404, HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist

from quiz.serializers import QuestionSerializer, QuizSerializer
from quiz.models import Question, Quiz, QuizResponse, Personality

from rest_framework.views import APIView
from rest_framework.response import Response

from scorer import scoring


class QuizQuestion(APIView):
    """
    Retrieve question instance.
    """
    def get_question(self, position):
        try:
            quiz = Quiz.objects.get(name="16 TP Test")
            question = quiz.questions.get(position=position)
            return question
        except Question.DoesNotExist:
            raise Http404

    def set_postion(self, fbid, position):
        quiz_response = QuizResponse.objects.get(responder=fbid)
        quiz_response.position = position
        quiz_response.save()

    def get(self, request, fbid, position, format=None):
        question = self.get_question(position)
        self.set_postion(fbid, position)
        serializer = QuestionSerializer(question)
        return JsonResponse(serializer.data)


class QuizDetails(APIView):
    def get_object(self, qid):
        try:
            quiz = Quiz.objects.get(pk=qid)
            return quiz
        except Quiz.DoesNotExist:
            raise Http404

    def get(self, request, qid, format=None):
        quiz = self.get_object(qid)
        serializer = QuizSerializer(quiz)
        return JsonResponse(serializer.data)


def StartQuiz(request, fbid):
    quiz = Quiz.objects.get(name="16 TP Test")
    quiz_response, created = QuizResponse.objects.get_or_create(quiz=quiz, responder=fbid, started=True)
    if created:
        index = 1
    else:
        index = quiz_response.position

    return JsonResponse({'status': 'Success', 'index': index})

def RestartQuiz(request, fbid):
    try:
        quiz_response = QuizResponse.objects.get(responder=fbid)
        quiz_response.position = 1
        quiz_response.save()
    except ObjectDoesNotExist:
        pass

    return JsonResponse({'status': 'Success', 'index': 0})

def submit_answer(request, fbid, answer_code):
    quiz_response = QuizResponse.objects.get(responder=fbid)
    position = quiz_response.position
    question_id = quiz_response.quiz.questions.get(position=position).id
    quiz_response.answers.create(question_id=question_id, answer_choice=answer_code)

    next_position = position + 1
    quiz_length = quiz_response.quiz.length()

    if next_position <= quiz_length:
        quiz_response.position += 1
        quiz_response.save()
        return JsonResponse({'status': 'Ongoing', 'next_question': position + 1})
    else:
        quiz_response.finished = True
        answers = quiz_response.answers.all()
        qanda = {}

        for ans in answers:
            qanda[ans.question_id] = ans.answer_choice


        personality = scoring(qanda)
        personality_type = Personality.objects.get(name=personality)
        description = personality_type.description
        careers = personality_type.careers.all()

        careerlist = []
        for career in careers:
            careerlist.append(career.name)

        quiz_response.result = personality
        quiz_response.save()

        return JsonResponse({
            'status': "Complete",
            'personality': personality,
            'description': description,
            'careers': careerlist
        })


def UserPersonality(request, fbid):
    quiz_response = QuizResponse.objects.get(responder=fbid)
    personality = quiz_response.result
    personality_type = Personality.objects.get(name=personality)
    careers = personality_type.careers.all()
    careerlist = []

    for career in careers:
        careerlist.append(career.name)

    return JsonResponse({
        'personality': personality,
        'careers': careerlist
    })
