# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0006_auto_20170414_1851'),
    ]

    operations = [
        migrations.RenameField(
            model_name='quizresponse',
            old_name='quiz_position',
            new_name='position',
        ),
    ]
