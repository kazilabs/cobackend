# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0005_auto_20170411_2254'),
    ]

    operations = [
        migrations.AddField(
            model_name='quizresponse',
            name='result',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='quizresponse',
            name='responder',
            field=models.CharField(unique=True, max_length=128),
        ),
    ]
